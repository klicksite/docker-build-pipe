FROM atlassian/pipelines-awscli

RUN apk update && apk add --no-cache docker

COPY pipe.sh /

RUN chmod +x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
