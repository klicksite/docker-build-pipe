#!/bin/bash

ECR_REGISTRY_URL=$ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com

DOCKER_IMAGE_NAME=$ECR_REGISTRY_URL/$APP_NAME:$GIT_COMMIT
 
docker build -t $DOCKER_IMAGE_NAME .

eval $(aws ecr get-login --region ${AWS_DEFAULT_REGION} --no-include-email)

docker push $DOCKER_IMAGE_NAME

docker save --output tmp-image.docker $DOCKER_IMAGE_NAME
