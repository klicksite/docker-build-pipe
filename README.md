# Docker Build Pipe

```yml
- pipe: docker://klickpages/docker-build-pipe:latest
  variables:
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
    AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
    ACCOUNT_ID: $AWS_ECR_ACCOUNT_ID
    APP_NAME: application/name
    GIT_COMMIT: ${BITBUCKET_COMMIT::7}
```
